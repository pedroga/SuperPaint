#define _GNU_SOURCE
#include <stdarg.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include "drawgon.h"

typedef double **Matrix;
typedef struct { double x; double y; } Point;
typedef enum { LINE, POLYGON, CIRCLE } shape_t;
typedef struct shape *Shape;
struct shape {
    shape_t type;
    Matrix mobj;
};

static GList *SHAPEL = NULL;
static GList *SELECL = NULL;

static Matrix matrix_create(int points)
{
    int i;
    Matrix m = malloc(sizeof(double *) * 3);
    for (i = 0; i < 3; ++i)
	m[i] = calloc(sizeof(double), points + 1);
    for (i = 0; i < points; ++i)
	m[2][i] = 1;
    return m;
}

static void matrix_multiply(Shape s, Matrix m)
{
    Matrix ma;
    Matrix mo = s->mobj;
    int c, i, j, k;

    for (c = 0; mo[2][c]; ++c)
	;
    ma = matrix_create(c);
    for (i = 0; i < 3; ++i)
	for (j = 0; j < c; ++j) {
	    ma[i][j] = 0;
	    for (k = 0; k < 3; ++k)
		ma[i][j] += m[i][k] * mo[k][j];
	}
    s->mobj = ma;
    free(mo);
}

static void shapes_create(shape_t type, int points, ...)
{
    int i;
    Matrix m = matrix_create(points);
    va_list ap;
    Shape s = malloc(sizeof(struct shape));

    s->type = type;
    va_start(ap, points);
    for (i = 0; i < points; ++i) {
	Point p = va_arg(ap, Point);

	m[0][i] = p.x;
	m[1][i] = p.y;
    }
    va_end(ap);
    s->mobj = m;
    SHAPEL = g_list_prepend(SHAPEL, s);
}

static void shapes_draw(Shape s, cairo_t *cr)
{
    int i;
    Matrix m = s->mobj;

    cairo_new_path(cr);
    if (s->type == CIRCLE) {
	cairo_arc(cr, m[0][0], m[1][0],
		  sqrt(pow(m[0][1] - m[0][0], 2) + pow(m[1][1] - m[1][0], 2)),
		  0, 2*M_PI);
    } else {
	cairo_move_to(cr, m[0][0], m[1][0]);
	for (i = 1; m[2][i]; ++i) {
	    cairo_line_to(cr, m[0][i], m[1][i]);
	}
	if (POLYGON)
	    cairo_close_path(cr);
    }
    cairo_stroke(cr);
}

static void shapes_clear(void)
{
    void shape_remove(Shape s)
    {
	free(s->mobj);
	free(s);
    }
    
    g_list_foreach(SHAPEL, (GFunc)shape_remove, NULL);
    g_list_free(SHAPEL);
    SHAPEL = NULL;
}

static void shapes_clear_selection(void)
{
    g_list_free(SELECL);
    SELECL = NULL;
}

static void shapes_select(Point p1, Point p2)
{
    Point p;

    void select_between(Shape s)
    {
	Point p;
	int i;
	Matrix m = s->mobj;
	
	bool between(Point p, Point bet, Point ween)
	{
	    bool b = true;

	    if (p.x < bet.x) b = false;
	    else if (p.y < bet.y) b = false;
	    else if (p.x > ween.x) b = false;
	    else if (p.y > ween.y) b = false;

	    return b;
	}

	for (i = 0; m[2][i]; ++i) {
	    p.x = m[0][i];
	    p.y = m[1][i];
	    if (between(p, p1, p2)) {
		SELECL = g_list_prepend(SELECL, s);
		break;
	    }
	}
    }

    if (p1.x > p2.x) {
	p.x = p1.x;
	p1.x = p2.x;
	p2.x = p.x;
    }
    if (p1.y > p2.y) {
	p.y = p1.y;
	p1.y = p2.y;
	p2.y = p.y;
    }
    g_list_foreach(SHAPEL, (GFunc)select_between, NULL);
}

static void shapes_translate(double dx, double dy)
{
    static Matrix tm = NULL;

    if (!tm) {
	int i;

	tm = malloc(sizeof(double *) * 3);
	for (i = 0; i < 3; ++i)
	    tm[i] = malloc(sizeof(double) * 3);
	tm[0][0] = 1; tm[0][1] = 0; 
	tm[1][0] = 0; tm[1][1] = 1; 
	tm[2][0] = 0; tm[2][1] = 0; tm[2][2] = 1;
    }
    tm[0][2] = dx;
    tm[1][2] = dy;
    g_list_foreach(SELECL, (GFunc)matrix_multiply, tm);
}

static void shapes_scale(int sx, int sy)
{
    static Matrix sm = NULL;
    GList *iter;
    
    if (!sm) {
	int i;
	
	sm = malloc(sizeof(double *) * 3);
	for (i = 0; i < 3; ++i)
	    sm[i] = malloc(sizeof(double) * 3);
	sm[0][1] = 0;
	sm[1][0] = 0;
	sm[2][0] = 0; sm[2][1] = 0; sm[2][2] = 1;
    }
    sm[0][0] = sx;
    sm[1][1] = sy;

    for (iter = SELECL; iter; iter = iter->next) {
	Matrix m = ((Shape)iter->data)->mobj;
	int x = m[0][0];
	int y = m[1][0];

	sm[0][2] = x - x * sx;
	sm[1][2] = y - y * sy;
	matrix_multiply(iter->data, sm);
    }
}

static void shapes_rotate(double angle)
{
    static Matrix rm = NULL;
    double s, c;
    GList *iter;

    if (!rm) {
	int i;
	
	rm = malloc(sizeof(double *) * 3);
	for (i = 0; i < 3; ++i)
	    rm[i] = malloc(sizeof(double) * 3);
	rm[2][0] = 0; rm[2][1] = 0; rm[2][2] = 1;
    }
    
    for (iter = SELECL; iter; iter = iter->next) {
	Matrix m = ((Shape)iter->data)->mobj;
	int x = m[0][0];
	int y = m[1][0];

	angle = (angle/180) * M_PI;
	sincos(angle, &s, &c);
	rm[0][0] = c; rm[0][1] = -s; rm[0][2] = y * s - x * c + x;
	rm[1][0] = s; rm[1][1] = c; rm[1][2] = -x * s - y * c + y;
	matrix_multiply(iter->data, rm);
    }
}

void drawgon_draw_all(GtkWidget *widget, cairo_t *cr)
{
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_paint(cr);
    cairo_set_source_rgb(cr, 0, 0, 0);
    g_list_foreach(SHAPEL, (GFunc)shapes_draw, cr);
    cairo_set_source_rgb(cr, 1, 0.5, 0.5);
    g_list_foreach(SELECL, (GFunc)shapes_draw, cr);
    gtk_widget_queue_draw(widget);
}

bool drawgon_exec(const char *_cmd)
{
    char *cmd = strdup(_cmd);
    char *tok = strtok(cmd, ":");
    bool ok = false;

    Point get_point(void)
    {
	Point p;

	tok = strtok(NULL, "(");
	p.x = strtod(tok, &tok);
	p.y = strtod(tok + 1, &tok);
	
	return p;
    }

    if (!strcmp(tok, "line")) {
	Point p1, p2;

	p1 = get_point();
	p2 = get_point();
	shapes_create(LINE, 2, p1, p2);
	ok = true;
    } else if (!strcmp(tok, "circle")) {
        Point p1, p2;

	p1 = get_point();
	p2 = get_point();
	shapes_create(CIRCLE, 2, p1, p2);
	ok = true;
    } else if (!strcmp(tok, "rectangle")) {
	Point p1, p2, p3, p4;

	p1 = get_point();
	p3 = get_point();
	p2.x = p3.x; p2.y = p1.y;
	p4.x = p1.x; p4.y = p3.y;
	shapes_create(POLYGON, 4, p1, p2, p3, p4);
	ok = true;
    } else if (!strcmp(tok, "triangle")) {
	Point p1, p2, p3;

	p1 = get_point();
	p2 = get_point();
	p3 = get_point();
	shapes_create(POLYGON, 3, p1, p2, p3);
	ok = true;
    } else if (!strcmp(tok, "clear")) {
	shapes_clear_selection();
	shapes_clear();
	ok = true;
    } else if (!strcmp(tok, "sclear")) {
	shapes_clear_selection();
	ok = true;
    } else if (!strcmp(tok, "translate")) {
	Point p1 = get_point();

	shapes_translate(p1.x, p1.y);
	ok = true;
    } else if (!strcmp(tok, "rotate")) {
	double angle;
	sscanf(_cmd, "rotate:%lf", &angle);
	shapes_rotate(-angle);
	ok = true;
    } else if (!strcmp(tok, "scale")) {
	Point p1 = get_point();

	shapes_scale(p1.x, p1.y);
	ok = true;
    } else if (!strcmp(tok, "select")) {
	Point p1, p2;

	p1 = get_point();
	p2 = get_point();
	shapes_select(p1, p2);
	ok = true;
    } free(cmd);
    return ok;
}
